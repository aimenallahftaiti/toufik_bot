// Initialisazing vars
const Discord = require("discord.js");
const {token} = require("./config.json");
const client = new Discord.Client();
client.commands = new Discord.Collection();
var cron = require('node-cron');
// const { channel } = require("diagnostics_channel");

const role_to_mention = "751323792072245248";
const channel_to_ping = "751320593894473749";

// Starting server
client.on("ready", () => {
  cron.schedule('0 16 * * *', () => {
    client.channels.fetch(channel_to_ping)
    .then(c => {
        c.send("<@&" + role_to_mention + "> N'oubliez pas le TPA ;)");
    })
  });
});

client.login(token);